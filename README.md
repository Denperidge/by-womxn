# by-womxn

BADASS LIBRE FONTS BY WOMXN — This website aims at giving visibility to libre fonts drawn by womxn designers, who are often underrepresented in the traditionally conservative field of typography.